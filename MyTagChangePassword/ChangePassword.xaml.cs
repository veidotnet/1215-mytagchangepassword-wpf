﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyTagChangePassword
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        Tools tools = new Tools();
        Logging log = new Logging();

        public ChangePassword()
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;

            InitializeComponent();
            txtNewPassword.Focus();
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;
            log.LogException(ex.Message, ex.InnerException.Message);
            log.LogInfo("UnhandledException caught : " + ex.Message);
            log.LogInfo("UnhandledException StackTrace : " + ex.StackTrace);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            NextAction();
        }

        private void txtConfirmNewPasssword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                NextAction();
            }
        }

        private void txtNewPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                NextAction();
            }
        }

        private void NextAction()
        {
            bool result = false;

            try
            {
                if (string.IsNullOrEmpty(txtNewPassword.Password) || string.IsNullOrEmpty(txtConfirmNewPasssword.Password))
                {
                    log.LogInfo("Please check your password, it must not be empty");
                    MessageBox.Show("Password Fields cannot be Empty!", "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (txtNewPassword.Password.Trim() != txtConfirmNewPasssword.Password.Trim())
                {
                    log.LogInfo("Password not Match!");
                    MessageBox.Show("Password not Match!", "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    if (txtNewPassword.Password.Trim() == txtConfirmNewPasssword.Password.Trim())
                    {
                        string username = MainWindow.account.Trim();
                        string password = txtConfirmNewPasssword.Password.Trim();

                        log.LogInfo("Password Match!, now Updating...");
                        result = tools.Encrypt(username, password);

                        if (result)
                        {
                            var res = MessageBox.Show("Password has been successfully Changed! Click 'OK' to Exit.", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                            log.LogInfo("Password has been successfully Changed!");

                            if (res == MessageBoxResult.OK)
                            {
                             
                                Environment.Exit(0);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Password save Failed! Please Check the Log file.", "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            log.LogInfo("Password save Failed! Please check BackupFile and Ecnrypted File!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Password save Failed ! Please Check the Log file.", "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }

                }
            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.Message);
                MessageBox.Show("Exception Encounterred..." + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void winChangePass_Closing(object sender, CancelEventArgs e)
        {
            var msg = MessageBox.Show(this, "Are You sure you want to Exit?", "Closing Application", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            if (msg != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                Environment.Exit(0);
            }
        }

        String SecureStringToString(SecureString value)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        private void ChkboxShowPassword_Checked(object sender, RoutedEventArgs e)
        {
            CopyPasswordContent(_passwordBox: txtNewPassword, _textBox: txtNewPassword_Copy);
            CopyPasswordContent(_passwordBox: txtConfirmNewPasssword, _textBox: txtConfirmNewPasssword_Copy);
        }

        void CopyPasswordContent(PasswordBox _passwordBox, TextBox _textBox)
        {
            var secureString = new SecureString();
            foreach (var c in _passwordBox.Password)
            {
                secureString.AppendChar(c);
            }
            _textBox.Text = SecureStringToString(secureString);
        }
    }

}
