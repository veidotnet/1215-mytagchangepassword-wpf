﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTagChangePassword
{
    public class Logging
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("SystemLogger");

        public void LogInfo(string value)
        {
            logger.Info(value);
        }

        public void LogException(string exMessage,string innerEx)
        {
            logger.Info("Exception Encountered:" + exMessage);
            logger.Info(innerEx);
        }
    }
}
