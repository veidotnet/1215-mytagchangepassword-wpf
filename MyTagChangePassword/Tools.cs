﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MyTagChangePassword
{
    public class Tools
    {
        Logging log = new Logging();
        AppSettingsReader appsettingsReader = new AppSettingsReader();
        List<string> readLines = new List<string>();

        public bool Encrypt(string account,string toEncryptValue)
        {
            string encryptedString = string.Empty;
            bool result = false;

            try
            {
                byte[] keyArray = UTF8Encoding.UTF8.GetBytes("PPxsAOVNtNAFeYQq");
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncryptValue);
                byte[] bytes = Encoding.Unicode.GetBytes(toEncryptValue);

                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                rDel.Padding = PaddingMode.PKCS7;

                ICryptoTransform crypto = rDel.CreateEncryptor();
                byte[] resultArray = crypto.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                encryptedString = Convert.ToBase64String(resultArray, 0, resultArray.Length);

                ValidateDirectories();
                result = UpdatePassword(account,encryptedString);
              
            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.Message);
                return result;
            }

           
            return result;
        }

        public bool UpdatePassword(string account,string encryptedPassword)
        {

            bool result = false;
            var appsettingsReader = new AppSettingsReader();

            try
            {
                string keyvalue = appsettingsReader.GetValue("FilePath", typeof(string)).ToString();
                result = MoveToBackup(keyvalue);
                if (result)
                {
                    log.LogInfo("Generating New file with the UpdatedPassword...");

                    result = WriteToFile(account, encryptedPassword);
                    //result = WriteToFile(readLines);
                    if (!result)
                    {
                        log.LogInfo("Error Writting to File!");
                    }
                }
                else
                {
                    log.LogInfo("Error Moving File to Backup Folder!");
                }
            }
            catch (Exception ex)
            {
                log.LogException("Exception Encountered on UpdatePassword() :" + ex.Message, ex.InnerException.Message);
            }
            

            return result;
        }
        public bool MoveToBackup(string sourcePath)
        {
            #region Variables
            bool result = false;
            string backupPath = appsettingsReader.GetValue("BackupFilePath", typeof(string)).ToString();
            string defaultFileName = appsettingsReader.GetValue("DefaultFileName", typeof(string)).ToString();
            string filePath = sourcePath + defaultFileName;
            string shortDate = DateTime.Now.ToString();
            shortDate = DateTime.Parse(shortDate).ToString("yyyyMMdd HHmmss");
            string fileDest = backupPath + Path.GetFileNameWithoutExtension(defaultFileName) + "_" + shortDate + Path.GetExtension(defaultFileName);
            #endregion

            try
            {
                if (Directory.Exists(sourcePath))
                {
                    if (Directory.Exists(backupPath))
                    {
                        if (File.Exists(filePath))
                        {
                            log.LogInfo("Copying Old file to Backup Folder... " + backupPath);

                            if (CanReadFile(filePath))
                            {
                                log.LogInfo("Now Transferring File to... " + backupPath);
        
                                string[] fileRows = File.ReadAllLines(filePath);
                                readLines = new List<string>(fileRows);
                                using (StreamWriter file = new StreamWriter(fileDest, true))
                                {
                                    log.LogInfo("Writing File to :" + backupPath);
                                    foreach (var item in readLines)
                                    {
                                        file.WriteLine(item);
                                    }
                                }

                                //File.Copy(filePath, fileDest);

                                log.LogInfo("Now Checking File Copied... " + fileDest);
                                if (File.Exists(fileDest))
                                {
                                    log.LogInfo("Now Checking File if still on Source... " + fileDest);
                                    if (CanReadFile(filePath))
                                    {
                                        File.Delete(filePath);

                                        if (!File.Exists(filePath))
                                        {
                                            result = true;
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                    }
                                }
                                else
                                {
                                    log.LogInfo("File not found!... " + filePath);
                                }

                            }
                            else
                            {
                                log.LogInfo("File Can't be Accessed!... " + filePath);
                            }

                        }
                        else
                        {
                            log.LogInfo("File not found!... " + filePath);
                        }
                    }
                    else
                    {
                        log.LogInfo("Path not found!... " + backupPath);
                    }
                }
                else
                {
                    log.LogInfo("Path not found!... " + sourcePath);
                }

            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.Message);
                result = false;
            }

            if (result)
            {
                log.LogInfo("Moving Old file to Backup Folder Success!... " + backupPath);
            }
            else
            {
                log.LogInfo("Moving Old file to Backup Folder Failed!... " + backupPath);
                File.Delete(fileDest);
            }

            return result;
        }

        public bool WriteToFile(string username, string password)
        {
            #region Variable
            bool result = false;
            string sourcePath = appsettingsReader.GetValue("FilePath", typeof(string)).ToString();
            string backupPath = appsettingsReader.GetValue("BackupFilePath", typeof(string)).ToString();
            string defaultFileName = appsettingsReader.GetValue("DefaultFileName", typeof(string)).ToString();
            string filePath = sourcePath + "\\" + defaultFileName;
            string fileDest = backupPath + "\\" + defaultFileName;
            #endregion

            try
            {
                using (StreamWriter file = new StreamWriter(filePath, true))
                {
                    log.LogInfo("Writing File to :" + sourcePath);
                    file.WriteLine(username);
                    file.WriteLine(password);
                    file.Close();
                }

                result = true;
            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.Message);
                result = false;
            }

            if (result)
            {
                log.LogInfo("Writing File Success!... " + sourcePath);
            }
            else
            {
                log.LogInfo("Writing File Failed!... " + sourcePath);
            }

            return result;
        }

        public bool WriteToFile(string[] userPass)
        {
            #region Variable
            bool result = false;
            string sourcePath = appsettingsReader.GetValue("FilePath", typeof(string)).ToString();
            string backupPath = appsettingsReader.GetValue("BackupFilePath", typeof(string)).ToString();
            string defaultFileName = appsettingsReader.GetValue("DefaultFileName", typeof(string)).ToString();
            string filePath = sourcePath + "\\" + defaultFileName;
            string fileDest = backupPath + "\\" + defaultFileName;
            #endregion

            try
            {
                using (StreamWriter file = new StreamWriter(filePath, true))
                {
                    log.LogInfo("Writing File to :" + sourcePath);
                    foreach (var item in userPass)
                    {
                        file.WriteLine(item);
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.Message);
                result = false;
            }

            if (result)
            {
                log.LogInfo("Writing File Success!... " + sourcePath);
            }
            else
            {
                log.LogInfo("Writing File Failed!... " + sourcePath);
            }

            return result;
        }

        public void ValidateDirectories()
        {
            List<string> directories = new List<string>();

            try
            {
                directories.Add(appsettingsReader.GetValue("FilePath", typeof(string)).ToString());
                directories.Add(appsettingsReader.GetValue("BackupFilePath", typeof(string)).ToString());

                log.LogInfo("Checking Directories...");
                //Folder Checking/Creation
                foreach (var dir in directories)
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogException("Exception Encountered on ValidateDirectories() :" + ex.Message, ex.InnerException.Message);
            }
}

        private void RestartWindowsService(string serviceName)
        {
            try
            {
                log.LogInfo("Restarting Service:" + serviceName);
                System.Diagnostics.Process.Start("net", "stop " + serviceName);
                System.Diagnostics.Process.Start("net", "start " + serviceName);
            }
            catch (Exception ex)
            {
                log.LogException(ex.Message, ex.InnerException.ToString());
            }
        }

        private bool CanReadFile(string filePath)
        {

            try
            {
                log.LogInfo("Checking File if it accessible..." + filePath);
                //are left undisposed.
                using (FileStream fileStream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    if (fileStream != null) fileStream.Close();  
                }
            }
            catch (IOException ex)
            {
                return false;
            }

            return true;
        }
    }

    
}
