﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyTagChangePassword
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string account = string.Empty;
        Logging log = new Logging();

        public MainWindow()
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;

            InitializeComponent();
            txtAccount.Focus();
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;
            log.LogException(ex.Message, ex.InnerException.Message);
            log.LogInfo("UnhandledException caught : " + ex.Message);
            log.LogInfo("UnhandledException StackTrace : " + ex.StackTrace);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            NextAction();
        }

        private void txtAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                NextAction();
            }
        }

        private void NextAction()
        {
            if (!string.IsNullOrEmpty(txtAccount.Text))
            {
                account = txtAccount.Text;
                ChangePassword changePassword = new ChangePassword();
                this.Hide();
                changePassword.Show();
            }
            else
            {
                MessageBox.Show("Account Field Cannot be Empty!", "Error!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void winMainWindow_Closing(object sender, CancelEventArgs e)
        {
            var msg = MessageBox.Show(this, "Are You sure you want to Exit?", "Closing Application", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            if (msg != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }
}
